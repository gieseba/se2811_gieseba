import javafx.scene.image.Image;

/**
 * @author dennyb
 * @version 1.0
 * @created 19-Dec-2017 6:20:23 PM
 * This is class is a sub class of the flower and one of the
 * implementations of our flowers on the game this one gives the most
 * nectar when a bee stops by it
 */
public class BadFlower extends Flower {

    private Image flowerImageReference;
	private String ID;
	private int amountOfNectarAvalible = 60;
	private final int nectarAmountToGive = -3;
	private int xLocation;
	private int yLocation;

    /**
     * this is the constructor for the big beeImage
	 * @author dennyb
     * @param xLocation the x location the flowerImageReference will be displayed
     * @param yLocation the y location the flowerImageReference will be displayed
     * @param ID the Id  so you can distinguish this flowerImageReference from the others
     * @param flowerImageReference the reference to the image that represents this flowerImageReference
     */
	public BadFlower(int xLocation, int yLocation, String ID, Image flowerImageReference, String flowerID){
	    this.flowerImageReference = flowerImageReference;
	    this.ID = ID;
	    this.xLocation = xLocation;
	    this.yLocation = yLocation;
	}

	/**
	 * this give the amount of nectar that a flower can give to a bee
	 * @return The amount of nectar for this flower to give
	 */
	@Override
	public int giveNectar(){
		if (amountOfNectarAvalible != 0) {
			amountOfNectarAvalible = amountOfNectarAvalible + nectarAmountToGive;
			return nectarAmountToGive;
		}
		return 0;

	}

	@Override
	public int getXLocation() {
		return xLocation;
	}

	@Override
	public int getYLocation() {
		return yLocation;
	}

	@Override
	public Image getFlowerImageReference() {
		return flowerImageReference;
	}

	@Override
	public int getAmountOfNectarAvailable() {
		return amountOfNectarAvalible;
	}

}