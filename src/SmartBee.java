import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author dennyb
 * @version 1.0
 * @created 19-Dec-2017 6:20:23 PM
 * This Class is one of the beeImage implementations it is the fast beeImage so it
 * goes farther then both the slow beeImage and the normal beeImage but to make up
 * for that it has less energy then the slow beeImage and the normal beeImage
 */
public class SmartBee extends Bee {

    private static final int RANGE = 100;
    private static final int CANVAS_WIDTH = 800;
    private static final int CANVAS_HEIGHT = 516;
    private Image beeImageReference;
    private ProgressBar energyBarReference;
    private String ID;
    private final int maximumEnergyLevel = 30;
    private int energyLevel = maximumEnergyLevel;
    private int xLocation;
    private int yLocation;
    private Flower closestFlower;
    private boolean isWrapped = false;

    /**
     * this is the constructor for SmartBee
     *
     * @param xLocation          starting x Location for Bee
     * @param yLocation          starting y Location for Bee
     * @param beeImageReference  the reference to the image that represents this beeImage
     * @param energyBarReference the reference to the energy bar that represents this beeImage
     * @param ID                 this is the ID of this beeImage so it can be distinguished between other beeImage's
     * @author dennyb
     */
    public SmartBee(int xLocation, int yLocation, Image beeImageReference, ProgressBar energyBarReference, String ID) {
        this.ID = ID;
        this.energyBarReference = energyBarReference;
        this.beeImageReference = beeImageReference;
        this.yLocation = yLocation;
        this.xLocation = xLocation;
    }

    @Override
    public boolean isWrapped() {
        return isWrapped;
    }

    @Override
    public void setWrapped(boolean bool) {
        isWrapped = bool;
    }

    @Override
    public int getXLocation() {
        return xLocation;
    }

    @Override
    public int getYLocation() {
        return yLocation;
    }

    @Override
    public int getEnergyLevel() {
        return energyLevel;
    }

    @Override
    public void setEnergyBarReference(ProgressBar bar) {
        energyBarReference = bar;
    }

    @Override
    public ProgressBar getEnergyBarReference() {
        return energyBarReference;
    }

    @Override
    public Image getBeeImageReference() {
        return beeImageReference;
    }

    @Override
    public void setBeeImageReference(Image image) {
        beeImageReference = image;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public int getEnergyFromFlower(int energyAmount) {
        return energyLevel += energyAmount;
    }

    /**
     * @author gieseba
     * When this method is called it changes the location of the bee
     */
    @Override
    public void movement() {
        int currentX = xLocation;
        int currentY = yLocation;
        int flowerX = CANVAS_WIDTH / 2;
        int flowerY = CANVAS_HEIGHT / 2;
        if (closestFlower != null) {
            flowerX = closestFlower.getXLocation();
            flowerY = closestFlower.getYLocation();
        }

        xLocation = flowerX;
        yLocation = flowerY;
    }


    /**
     * random number generator for a certain range
     *
     * @return the random number
     */
    private int generateNumInRange() {
        Random random = new Random();
        return random.nextInt(RANGE + 1 + RANGE) - RANGE;
    }

    public void setClosestFlower(Flower flower) {
        closestFlower = flower;
    }

    @Override
    public Flower getClosestFlower() {
        return closestFlower;
    }

    @Override
    public Flower findClosestFlower(BeeInterface bee) {
        return closestFlower;
    }


}
