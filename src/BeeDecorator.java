import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;

public class BeeDecorator implements BeeInterface{

    private BeeInterface beeWrapped;
    private boolean isWrapped = false;

    public BeeDecorator(BeeInterface bee) {
        this.beeWrapped = bee;
    }

    @Override
    public boolean isWrapped() {
        return isWrapped;
    }

    @Override
    public void setWrapped(boolean bool) {
        isWrapped = bool;
    }

    @Override
    public int getXLocation() {
        return xLocation;
    }

    @Override
    public void movement() {
        beeWrapped.movement();
    }

    @Override
    public int getEnergyFromFlower(int nectar) {
        return beeWrapped.getEnergyFromFlower(nectar);
    }

    @Override
    public void setClosestFlower(Flower flower) {

    }

    @Override
    public Flower getClosestFlower() {
        return beeWrapped.getClosestFlower();
    }

    @Override
    public Flower findClosestFlower(BeeInterface bee) {
        return beeWrapped.findClosestFlower(bee);
    }

    @Override
    public int getYLocation() {
        return yLocation;
    }

    @Override
    public int getEnergyLevel() {
        return energyLevel;
    }


    @Override
    public ProgressBar getEnergyBarReference() {
        return energyBarReference;
    }

    @Override
    public void setEnergyBarReference(ProgressBar bar) {
        beeWrapped.setEnergyBarReference(bar);
    }

    @Override
    public Image getBeeImageReference() {
        return beeImageReference;
    }

    @Override
    public void setBeeImageReference(Image image) {
        beeWrapped.setBeeImageReference(image);
    }

    @Override
    public String getID() {
        return ID;
    }

    public void setBeeWrapped(Bee bee) {
        this.beeWrapped = bee;
    }

    public BeeInterface getBeeWrapped() {
        return beeWrapped;
    }

}
