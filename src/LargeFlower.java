import javafx.scene.image.Image;

public class LargeFlower extends DecoratorFlower {

    private Image flowerImageReference;
    private String ID;
    private int xLocation;
    private int yLocation;
    private BeeDecorator decorator;

    public LargeFlower(int xLocation, int yLocation, String ID, Image flowerImageReference) {
        this.xLocation = xLocation;
        this.yLocation = yLocation;
        this.ID = ID;
        this.flowerImageReference = flowerImageReference;
        this.decorator = decorator;
    }

    @Override
    public BeeDecorator giveDecorator(BeeInterface bee) {
        decorator = new BeeDecorator(bee);
        return decorator;
    }

    @Override
    public int getXLocation() {
        return xLocation;
    }

    @Override
    public int getYLocation() {
        return yLocation;
    }

    @Override
    public Image getFlowerImageReference() {
        return flowerImageReference;
    }



}
