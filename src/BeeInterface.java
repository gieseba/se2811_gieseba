import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;

public interface BeeInterface {
    int RANGE = 200;
    int CANVAS_WIDTH = 800;
    int CANVAS_HEIGHT = 516;
    Image beeImageReference = new Image("bee.png");
    ProgressBar energyBarReference = new ProgressBar();
    String ID = "Bee";
    int maximumEnergyLevel = 20;
    int energyLevel = maximumEnergyLevel;
    int xLocation = CANVAS_WIDTH / 2;
    int yLocation = CANVAS_HEIGHT / 2;
    boolean isWrapped = false;

    boolean isWrapped();

    void setWrapped(boolean bool);

    int getXLocation();

    void movement();

    int getEnergyFromFlower(int nectar);

    void setClosestFlower(Flower flower);

    Flower getClosestFlower();

    Flower findClosestFlower(BeeInterface bee);

    int getYLocation();

    int getEnergyLevel();

    ProgressBar getEnergyBarReference();

    void setEnergyBarReference(ProgressBar bar);

    Image getBeeImageReference();

    void setBeeImageReference(Image image);

    String getID();
}
