import javafx.scene.image.Image;

import java.util.Random;

public class Large extends BeeDecorator {

    private BeeInterface wrappedBee;
    private Image largeImageReference = new Image("ScanBee.jpg");
    private int xLocation;
    private int yLocation;
    private boolean movingX;
    private boolean movingY;
    private int energyAmount = 100;
    private boolean isWrapped = false;

    public Large(BeeInterface bee) {
        super(bee);
        wrappedBee = bee;
    }

    @Override
    public boolean isWrapped() {
        return isWrapped;
    }

    @Override
    public void setWrapped(boolean bool) {
        isWrapped = bool;
    }

    @Override
    public void movement() {
        int currentX = xLocation;
        int currentY = yLocation;
        int addX = generateNumInRange();
        int[] returnedArray = movementHandler(currentX, currentY, addX);
        int updatedX = returnedArray[0];
        int updatedY = returnedArray[1];
        xLocation = updatedX;
        yLocation = updatedY;
    }

    private int generateNumInRange() {
        Random random = new Random();
        return random.nextInt(411) - 10;
    }

    private int[] movementHandler(int currentX, int currentY, int addedX) {
        int[] tempArray = new int[2]; //Array to return
        int finalX;
        int finalY = currentY;

        //Update X move.
        if(!movingX) {
            finalX = currentX + (addedX * -1);
        } else {
            finalX = currentX + addedX;
        }

        if(finalX > CANVAS_WIDTH) {

            int remainder = finalX % CANVAS_WIDTH - 50;
            finalX = CANVAS_WIDTH - remainder;
            movingX = false; //reverse scanBee movement.
            finalY = getFinalY(finalY);

        } else if(finalX < 0) {
            finalX = finalX * -1;
            movingX = true; //reverse scanBee movement.

            finalY = getFinalY(finalY);
        }

        tempArray[0] = finalX;
        tempArray[1] = finalY;
        return tempArray;
    }

    private int getFinalY(int finalY) {
        //Handle Y movement.
        if(!movingY) {
            finalY -= 15;
            if(finalY < 0) {
                finalY = finalY * -1;
                finalY = 15 - finalY;
                movingY = true;
            }
        } else {
            finalY += 15;
            if(finalY > CANVAS_HEIGHT) {
                int over = finalY % CANVAS_HEIGHT - 50;
                finalY = CANVAS_HEIGHT - over;
                movingY = false;
            }
        }
        return finalY;
    }

}
