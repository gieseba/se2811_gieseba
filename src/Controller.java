import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Controller implements Initializable {
    //wait for user input for tick??
    //event handler??
    @FXML
    public Pane pane;
    public Pane drawPane;
    public ScrollPane scrollPane;
    public Canvas gameCanvas;

    //this holds all the bees that are on the screen
    private ArrayList<BeeInterface> beeArrayList = new ArrayList<>();
    //this holds all the flowers that are on the screen
    private ArrayList<Flower> flowerArrayList = new ArrayList<>();
    //this is the image reference for all the bees
    private Image normalBeeImage = new Image("bee.png");
    private Image scanBeeImage = new Image("ScanBee.jpg");
    private Image smartBeeImage = new Image("SmartBee.jpg");
    //this is the image reference for the flowerImageReference
    private Image flowerImageReference = new Image("flower.png");
    private Image flowerImageReference1 = new Image("flower1.png");
    private Image flowerImageReference2 = new Image("flower2.png");
    private Image largeFlowerImageReference = new Image("largeflower.png");
    private Image shieldFlowerImageReference = new Image("shieldflower.png");
    private Image speedFlowerImageReference = new Image("speedflower.png");
    //graphic context for drawing on the canvas
    private GraphicsContext gc;

    private Image backgroundImage = new Image("background.jpg");

    private String gameInfo = "";
    private File file = new File("GameInfo.txt");

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        readGameInfo(file);
        gc = gameCanvas.getGraphicsContext2D();
        gc.drawImage(backgroundImage, 0, 0);
        createBees(25);
        populateBees();
        createFlowers(20);
        populateFlowers();
        //creates the EventHandler to handle the pressing of Keys
        pane.setOnKeyPressed(new EventHandler<KeyEvent>() {
            //this is the handler for key pressed
            @Override
            public void handle(KeyEvent keyEvent) {
                //gets the key code pressed
                KeyCode keyCode = keyEvent.getCode();
                //checks if the key is the up key
                if (keyCode == KeyCode.UP) {
                    moveBees();
                    // so the flowers are drawn back on to the screen because the
                    // screen gets cleared every time you move the bees
                    populateFlowers();
                }
            }
        });
    }


    /**
     * this creates all the bees that we are going to add to the screen
     *
     * @param amountOfBees The amount of bees we want to add to the screen
     */
    public void createBees(int amountOfBees) {
        for (int i = 0; i < amountOfBees; i++) {
            //the math random is done to 450 and 750 so the bees don't show up off of the canvas because their size is
            // 50 so 500 - 50 = 450 and 800 - 50 = 750 is the max they can be displayed
            ProgressBar progressBar = new ProgressBar();

            //NormalBee normalBee = new NormalBee((int) (Math.random() * 750),(int) (Math.random() * 450),normalBeeImage,progressBar,"NormalBee"+i);
            BeeInterface bee = createABee("Bee" + i, progressBar);
            beeArrayList.add(bee);
        }
    }

    /**
     * this creates all the bees that we are going to add to the screen
     *
     * @param amountOfFlowers The amount of bees we want to add to the screen
     */
    public void createFlowers(int amountOfFlowers) {
        for (int i = 0; i < amountOfFlowers; i++) {
            Flower flower = createAFlower("flowerImageReference" + i, "x");
            flowerArrayList.add(flower);
        }
    }

    /**
     * this adds the bees to the screen to start the game
     */
    public void populateBees() {
        int progressBarAmounts = 0;
        //pane to but the energy bars on
        Pane beeLevelPane = new Pane();
        //Acts as the container for the ProgressBar and HBox to fit on one line.
        ArrayList<HBox> container = new ArrayList<>();
        // holds all the energy bar of all the normalBeeImage's
        for (int i = 0; i < beeArrayList.size(); i++) {
            BeeInterface bee = beeArrayList.get(i);
            //checks if the energy level is above 0 else does not draw the normalBeeImage to "kill" it
            if (bee.getEnergyLevel() > 0) {
                GraphicsContext gc = gameCanvas.getGraphicsContext2D();
                //TODO if bee is large bee, then draw larger? Check using instanceOf??
                if(bee instanceof Large) {
                    gc.drawImage(bee.getBeeImageReference(), bee.getXLocation(), bee.getYLocation(), 100, 100);
                } else {
                    gc.drawImage(bee.getBeeImageReference(), bee.getXLocation(), bee.getYLocation(), 50, 50);
                }

                if (bee.getEnergyBarReference() == null) {
                    ProgressBar progressBar = new ProgressBar();
                    progressBar.setProgress(bee.getEnergyLevel() * 0.1);
                    bee.setEnergyBarReference(progressBar);
                }
                ProgressBar progressBar = bee.getEnergyBarReference();
                //setting there position on the scroll pane
                progressBar.setLayoutX(0);
                progressBar.setLayoutY(30 * i);
                //this must be a value of 1 or lower because it accepts a value from 0.0 - 1.0
                progressBar.setProgress(bee.getEnergyLevel() * 0.1);

                //creates the textArea that holds that normalBeeImage's data.

                TextArea textArea = new TextArea(bee.getID() + ": " + "X: " + bee.getXLocation() +
                        " " + "Y: " + bee.getYLocation() + "   Energy: " + bee.getEnergyLevel());
                gc.setStroke(Color.RED);
                gc.strokeText(bee.getID(), bee.getXLocation(), bee.getYLocation());
                textArea.setEditable(false);

                //Creates the HBox to hold the progress Bar and the textArea so it fits on one line.
                HBox hbox = new HBox();
                hbox.setLayoutX(0);
                hbox.setLayoutY(30 * i);
                hbox.setPrefHeight(20);
                hbox.getChildren().addAll(progressBar, textArea);
                container.add(hbox);
                progressBarAmounts = (30 * (i + 1));
            }
        }
        TextArea textArea = new TextArea(gameInfo);

        HBox hBox = new HBox();
        hBox.setLayoutX(0);
        hBox.setLayoutY(progressBarAmounts);
        hBox.getChildren().add(textArea);
        container.add(hBox);
        beeLevelPane.getChildren().addAll(container);
        textArea.setPrefWidth(800);
        textArea.setEditable(false);
        scrollPane.setContent(beeLevelPane);
    }

    /**
     * When called this method adds the flowers to the screen
     */
    public void populateFlowers() {
        for (int i = 0; i < flowerArrayList.size(); i++) {
            Flower flower = flowerArrayList.get(i);
            GraphicsContext gc = gameCanvas.getGraphicsContext2D();
            gc.drawImage(flower.getFlowerImageReference(), flower.getXLocation(), flower.getYLocation(), 50, 50);
        }
    }

    /**
     * When This method is called it clears the canvas of the bees
     * then calls the movement methods on all the bees in the array list
     * it then calls the populate method to redraws all the bees on the canvas
     */
    public void moveBees() {
        gc.clearRect(0, 0, gameCanvas.getWidth(), gameCanvas.getHeight());
        gc.drawImage(backgroundImage, 0, 0);
        updateSmartBees();
        for (int i = 0; i < beeArrayList.size(); i++) {
            BeeInterface bee = beeArrayList.get(i);
            bee.movement();
        }
        checkBeeEnergy();
        checkBeePowerUp();
        populateBees();
    }

    /**
     * Checks and handles normalBeeImage's energy levels after each turn.
     */
    public void checkBeeEnergy() {

        Flower closestFlower = null;
        double closestDistance = 0.0;

        for (BeeInterface bee : beeArrayList) {
            int beeXLocation = bee.getXLocation();
            int beeYLocation = bee.getYLocation();


            for (Flower flower : flowerArrayList) {
                int flowerXLocation = flower.getXLocation();
                int flowerYLocation = flower.getYLocation();

                double distance = calculateEuclideanDistance(beeXLocation, beeYLocation, flowerXLocation, flowerYLocation);
                if (distance < 30.0) {
                    if (closestFlower == null || distance < closestDistance) {
                        closestFlower = flower;
                        closestDistance = distance;
                    }
                }
            }
            if (closestFlower != null) {
                //TODO IF IT IS PROTECTED BEE, AND IT HAS SHIELD, THEN DEPLETE FROM SHIELD.

                if (bee instanceof Protected) {
                    if(((Protected) bee).getShieldPower() > 0) {
                        ((Protected) bee).damageShield(bee.getEnergyFromFlower(closestFlower.giveNectar()));
                    }
                } else {
                    bee.getEnergyFromFlower(closestFlower.giveNectar());
                }

                //bee.getEnergyFromFlower(closestFlower.giveNectar());
            } else {
                bee.getEnergyFromFlower(-1);
            }
        }

        //remove dead normalBeeImage's
        for (int i = 0; i < beeArrayList.size(); i++) {
            if (beeArrayList.get(i).getEnergyLevel() == 0) {
                beeArrayList.remove(i);
                i--;
            }
        }


        //loop through all flowers, save the flowerImageReference with the lowest distance, then check if that distance is within the range.
        //if distance is within range then give nectar, else energyLevel -1
    }

    /**
     * Calculates the Euclidean distance between two points.
     *
     * @param beeX    the bees current x location
     * @param beeY    the bees current y location
     * @param flowerX the flowers current location
     * @param flowerY the flower current locatino
     * @return double  the distance away
     */
    private double calculateEuclideanDistance(int beeX, int beeY, int flowerX, int flowerY) {
        double xDiff = beeX - flowerX;
        double xSqr = Math.pow(xDiff, 2);

        double yDiff = beeY - flowerY;
        double ySqr = Math.pow(yDiff, 2);

        double num = Math.sqrt(xSqr + ySqr);
        return num;
    }

    /**
     * @param id  The name name of the bee
     * @param bar the energy bar reference
     * @return a instance of the bee
     * @author dennyb
     */
    private BeeInterface createABee(String id, ProgressBar bar) {
        int beeDecider = (int) (Math.random() * 3);
        BeeInterface bee = null;
        int beeXCord = (int) (Math.random() * 750);
        int beeYCord = (int) (Math.random() * 450);
        if (beeDecider == 0) {
            bee = new ScanBee(beeXCord, beeYCord, scanBeeImage, bar, id);
        } else if (beeDecider == 2) {
            bee = new SmartBee(beeXCord, beeYCord, smartBeeImage, bar, id);
        } else {
            bee = new NormalBee(beeXCord, beeYCord, normalBeeImage, bar, id);
        }
        return bee;
    }

    /**
     * @param id       what to call the flower
     * @param flowerID
     * @return
     * @author gieseba
     */
    private Flower createAFlower(String id, String flowerID) {
        int flowerDecider = ThreadLocalRandom.current().nextInt(0, 6 + 1);
        Flower flower = null;
        int flowerXCord = (int) (Math.random() * 750);
        int flowerYCord = (int) (Math.random() * 450);
        if (flowerDecider == 0) {
            flower = new SmallFlower(flowerXCord, flowerYCord, id, flowerImageReference2, flowerID);
        } else if (flowerDecider == 1) {
            flower = new BadFlower(flowerXCord, flowerYCord, id, flowerImageReference, flowerID);
        } else if (flowerDecider == 2) {
            flower = new NormalFlower(flowerXCord, flowerYCord, id, flowerImageReference1, flowerID);
        } else if (flowerDecider == 3) {
            flower = new LargeFlower(flowerXCord, flowerYCord, id, largeFlowerImageReference);
        } else if (flowerDecider == 4) {
            flower = new SpeedFlower(flowerXCord, flowerYCord, id, speedFlowerImageReference);
        } else {
            flower = new ShieldFlower(flowerXCord, flowerYCord, id, shieldFlowerImageReference);
        }
        return flower;
    }


    /**
     * this reads in the file that is the description of the information on the
     * bees and the flowers and what they all are and there attributes
     *
     * @param file
     */
    private void readGameInfo(File file) {
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                gameInfo += scanner.nextLine() + "\n";
                System.out.println(gameInfo);
            }
        } catch (IOException ex) {
            System.out.println(" " + ex);
        }
    }

    private void updateSmartBees() {
        for (BeeInterface bee : beeArrayList) {
            if (bee instanceof SmartBee) {
                bee.setClosestFlower(findClosestFlower((Bee) bee));
            }
        }
    }

    private Flower findClosestFlower(Bee bee) {
        Flower closestFlower = null;
        double closestDistance = 0.0;

        for (Flower flower : flowerArrayList) {
            double distance = calculateEuclideanDistance(bee.getXLocation(), bee.getYLocation(), flower.getXLocation(), flower.getYLocation());
            if (bee.getClosestFlower() != flower) {
                if (closestFlower == null || distance < closestDistance) {
                    if (flower.getAmountOfNectarAvailable() > 0) {
                        closestFlower = flower;
                        closestDistance = distance;
                    }
                }
            }
        }
        return closestFlower;
    }

    private void checkBeePowerUp() {

        for (int i = 0; i < beeArrayList.size(); i++) {
            BeeInterface bee = beeArrayList.get(i);

            for (Flower flower : flowerArrayList) {

                if (flower instanceof DecoratorFlower) {
                    double dist = calculateEuclideanDistance(bee.getXLocation(), bee.getYLocation(),
                            flower.getXLocation(), flower.getYLocation());
                    if (dist < 25.0) {
                        beeArrayList.remove(i);

                        //TODO Wrap bee. Edit the list.
                        bee = ((DecoratorFlower) flower).giveDecorator(bee);
                        bee.setBeeImageReference(new Image("wrapped.jpg"));
                        bee.setWrapped(true);
                        beeArrayList.add(i, bee);
                    }
                }
            }
        }
    }
}