import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.w3c.dom.ranges.Range;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author dennyb
 * @version 1.0
 * @created 19-Dec-2017 6:20:23 PM
 * This Class is one of the beeImage implementations it is the normal beeImage so it
 * does not go as far as the fast beeImage but it goes farther then the slow beeImage
 * it has less energy then the slow beeImage but more energy than the fast beeImage
 */
public class NormalBee extends Bee {

    private static final int RANGE = 200;
    private static final int CANVAS_WIDTH = 800;
    private static final int CANVAS_HEIGHT = 516;
    private Image beeImageReference;
	private ProgressBar energyBarReference;
	private String ID;
	private final int maximumEnergyLevel = 20;
	private int energyLevel = maximumEnergyLevel;
	private int xLocation;
	private int yLocation;
	private Flower closestFlower;
	private boolean isWrapped = false;

    /**
     * this is the constructor for NormalBee
     * @author dennyb
     * @param xLocation starting x Location for Bee
     * @param yLocation starting y Location for Bee
     * @param beeImageReference the reference to the image that represents this beeImage
     * @param energyBarReference the reference to the energy bar that represents this beeImage
     * @param ID this is the ID of this beeImage so it can be distinguished between other beeImage's
     */
    public NormalBee(int xLocation, int yLocation, Image beeImageReference, ProgressBar energyBarReference, String ID){
        this.ID = ID;
        this.energyBarReference = energyBarReference;
        this.beeImageReference = beeImageReference;
        this.yLocation = yLocation;
        this.xLocation = xLocation;
    }

    @Override
    public int getEnergyFromFlower(int energyAmount){
        return energyLevel += energyAmount;
    }

    /**
     * @author gieseba
     * When this method is called it changes the location of the bee
     */
    @Override
    public void movement(){
        int currentX = xLocation;
        int currentY = yLocation;
        int addX = generateNumInRange();
        int addY = generateNumInRange();
        int updatedX = offSceneCheck(currentX,addX,CANVAS_WIDTH - 50);
        int updatedY = offSceneCheck(currentY,addY,CANVAS_HEIGHT - 50);
        xLocation = updatedX;
        yLocation = updatedY;

    }

    @Override
    public boolean isWrapped() {
        return isWrapped;
    }

    @Override
    public void setWrapped(boolean bool) {
        isWrapped = bool;
    }

    public int getXLocation() {
        return xLocation;
    }

    public int getYLocation() {
        return yLocation;
    }

    public int getEnergyLevel() {
        return energyLevel;
    }

    @Override
    public void setEnergyBarReference(ProgressBar bar) {
        energyBarReference = bar;
    }

    @Override
    public ProgressBar getEnergyBarReference() {
        return energyBarReference;
    }

    //This was just done real quick to see of everything was working
    public Image getBeeImageReference() {
        return beeImageReference;
    }

    @Override
    public void setBeeImageReference(Image image) {
        beeImageReference = image;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void setClosestFlower(Flower flower) {
        closestFlower = flower;
    }

    @Override
    public Flower getClosestFlower() {
        return closestFlower;
    }

    @Override
    public Flower findClosestFlower(BeeInterface bee) {
        return closestFlower;
    }

    /**
     * @author gieseba
     * This checks to make sur ethat the bee does not go off the screen
     */
    private int offSceneCheck(int current,int added, int canvasLengthElement) {
        int num = current + added;
        if(num > canvasLengthElement) {
            int remainder = num % canvasLengthElement;
            return canvasLengthElement - remainder;
        } else if (num < 0) {
            return num * -1;
        } else {
            return current + added;
        }
    }


    /**
     * random number generator for a certain range
     * @return the random number
     */
    private int generateNumInRange() {
        Random random = new Random();
        return random.nextInt(RANGE + 1 + RANGE) - RANGE;
    }

}