import javafx.scene.image.Image;

/**
 * @author dennyb
 * @version 1.0
 * @created 19-Dec-2017 6:20:23 PM
 */
public abstract class Flower {

	private Image flowerImageReference;
	private String ID;
	private int amountOfNectarAvailable;
	private int nectarAmountToGive;
	private int xLocation;
	private int yLocation;

	public Flower() {

    }

	/*
     If beeImage is in range, call giveNectar.
     If nectarAmountToGive > 0, then giveNectar.
     Decrease the nectarAmount by one.
    */
    public abstract int giveNectar();

    public abstract int getXLocation();

    public abstract int getYLocation();

    public abstract Image getFlowerImageReference();

    public abstract int getAmountOfNectarAvailable();

}
