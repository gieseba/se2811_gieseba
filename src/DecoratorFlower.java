import javafx.scene.image.Image;

public class DecoratorFlower extends Flower {

    private Image flowerImageReference;
    private String ID;
    private int amountOfDecoratorsAvailable;
    private int xLocation;
    private int yLocation;
    private BeeDecorator decorator;


    public BeeDecorator giveDecorator(BeeInterface bee) {
        decorator = new BeeDecorator(bee);
        return decorator;
    }

    @Override
    public int giveNectar() {
        return -1;
    }

    @Override
    public int getXLocation() {
        return xLocation;
    }

    @Override
    public int getYLocation() {
        return yLocation;
    }

    @Override
    public Image getFlowerImageReference() {
        return flowerImageReference;
    }

    @Override
    public int getAmountOfNectarAvailable() {
        return amountOfDecoratorsAvailable;
    }


}
