import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;

import java.util.Random;

/**
 * @author dennyb
 * @version 1.0
 * @created 19-Dec-2017 6:20:23 PM
 * This Class is one of the beeImage implementations it is the slower beeImage so it
 * does not go as far as the normal or fast beeImage but it has a higher starting
 * energy level then the other two beeImage's
 */
public class ScanBee extends Bee {

    private static final int RANGE = 100;
    private static final int CANVAS_WIDTH = 800;
    private static final int CANVAS_HEIGHT = 516;
    private Image beeImageReference;
    private ProgressBar energyBarReference;
    private String ID;
    private final int maximumEnergyLevel = 10;
    private int energyLevel = maximumEnergyLevel;
    private int xLocation;
    private int yLocation;
    private boolean movingX;  //if True then the bee is moving right
    private boolean movingY;  //if True then the bee is moving up
    private Flower closestFlower;
    private boolean isWrapped = false;

    /**
     * this is the constructor for ScanBee
     * @author dennyb
     * @param xLocation starting x Location for Bee
     * @param yLocation starting y Location for Bee
     * @param beeImageReference the reference to the image that represents this beeImage
     * @param energyBarReference the reference to the energy bar that represents this beeImage
     * @param ID this is the ID of this beeImage so it can be distinguished between other beeImage's
     */
    public ScanBee(int xLocation, int yLocation, Image beeImageReference, ProgressBar energyBarReference, String ID){
        this.ID = ID;
        this.energyBarReference = energyBarReference;
        this.beeImageReference = beeImageReference;
        this.yLocation = yLocation;
        this.xLocation = xLocation;
    }

    @Override
    public boolean isWrapped() {
        return isWrapped;
    }

    @Override
    public void setWrapped(boolean bool) {
        isWrapped = bool;
    }

    @Override
    public int getXLocation() {
        return xLocation;
    }

    @Override
    public int getYLocation() {
        return yLocation;
    }

    @Override
    public int getEnergyLevel() {
        return energyLevel;
    }

    @Override
    public void setEnergyBarReference(ProgressBar bar) {
        energyBarReference = bar;
    }

    @Override
    public ProgressBar getEnergyBarReference() {
        return energyBarReference;
    }

    @Override
    public Image getBeeImageReference() {
        return beeImageReference;
    }

    @Override
    public void setBeeImageReference(Image image) {
        beeImageReference = image;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void setClosestFlower(Flower flower) {
        closestFlower = flower;
    }

    @Override
    public Flower getClosestFlower() {
        return closestFlower;
    }

    @Override
    public Flower findClosestFlower(BeeInterface bee) {
        return closestFlower;
    }

    @Override
    public int getEnergyFromFlower(int energyAmount){
        return energyLevel += energyAmount;
    }

    /**
     * @author gieseba
     * When this method is called it changes the location of the bee
     */
    @Override
    public void movement(){
        int currentX = xLocation;
        int currentY = yLocation;
        int addX = generateNumInRange();
        int[] returnedArray = movementHandler(currentX, currentY, addX);
        int updatedX = returnedArray[0];
        int updatedY = returnedArray[1];
        xLocation = updatedX;
        yLocation = updatedY;
    }

    /**
     * random number generator for a certain range
     * @return the random number
     */
    private int generateNumInRange() {
        Random random = new Random();
        return random.nextInt(211) - 10;
    }

    /**
     * Handles the bee's movement and checks if it is off screen, if so then it inverts its movement.
     * @param currentX
     * @param currentY
     * @param addedX
     * @return
     */
    private int[] movementHandler(int currentX, int currentY, int addedX) {
        int[] tempArray = new int[2]; //Array to return
        int finalX;
        int finalY = currentY;

        //Update X move.
        if(!movingX) {
            finalX = currentX + (addedX * -1);
        } else {
            finalX = currentX + addedX;
        }

        if(finalX > CANVAS_WIDTH) {

            int remainder = finalX % CANVAS_WIDTH - 50;
            finalX = CANVAS_WIDTH - remainder;
            movingX = false; //reverse scanBee movement.
            finalY = getFinalY(finalY);

        } else if(finalX < 0) {
            finalX = finalX * -1;
            movingX = true; //reverse scanBee movement.

            finalY = getFinalY(finalY);
        }

        tempArray[0] = finalX;
        tempArray[1] = finalY;
        return tempArray;
    }

    private int getFinalY(int finalY) {
        //Handle Y movement.
        if(!movingY) {
            finalY -= 15;
            if(finalY < 0) {
                finalY = finalY * -1;
                finalY = 15 - finalY;
                movingY = true;
            }
        } else {
            finalY += 15;
            if(finalY > CANVAS_HEIGHT) {
                int over = finalY % CANVAS_HEIGHT - 50;
                finalY = CANVAS_HEIGHT - over;
                movingY = false;
            }
        }
        return finalY;
    }
}