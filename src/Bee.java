import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;


/**
 * @author dennyb
 * @version 1.0
 * @created 19-Dec-2017 6:20:23 PM
 */
public abstract class Bee implements BeeInterface{

    private static final int RANGE = 200;
    private static final int CANVAS_WIDTH = 800;
    private static final int CANVAS_HEIGHT = 516;
    private Image beeImageReference;
    private ProgressBar energyBarReference;
    private String ID;
    private static int maximumEnergyLevel = 20;
    private int energyLevel = maximumEnergyLevel;
    private int xLocation;
    private int yLocation;

    public Bee() {
      /*
      Create a random position on the board, where the starting xLocation and yLocation should be.
      Check if a beeImage is already there? Or unneeded.
      */
    }

    public abstract int getEnergyFromFlower(int energyAmount);

    /*
    Check beeImage's energy bar to see if it had just died....
    IF NOT:
    random number generator using the beeImage's maximumDistance it can travel.
    After move to different position use the list of flowers (make one) that holds all flowers
    Use this list to run through all flowers and compare the beeImage's location to the
    flowers range in, if in range, decrease what is needed(beeImage's energy?, flowers energy?)
    Do this for every beeImage.
    Possibly using a for loop? Or just use getEnergyFromFlower for each beeImage.
    */
    public abstract void movement();
  /*
  Random Number Generated Movement. Generate the x and y for the ++ movement, add that to the current x and y
  coordinate to get its new location. Call getEnergyFromFlower() that checks the range and updates the energy.
  Fast Bee(Note: drawPane is 500x800 px. Fast beeImage range = 300px ??
  Normal Bee(Range = 200 px??)
  Slow Bee(Range = 100 px??) 
  */


	public abstract int getXLocation();

    public abstract int getYLocation();

    public abstract int getEnergyLevel();

    public abstract void setEnergyBarReference(ProgressBar bar);

    public abstract ProgressBar getEnergyBarReference();

    public abstract Image getBeeImageReference();

    public abstract void setBeeImageReference(Image image);

    public abstract String getID();

    public abstract void setClosestFlower(Flower flower);

    public abstract Flower getClosestFlower();

}
